# Turntable Interface 

Documentation of the turntable interface based on DOIP.

Turntable acts as a facade for multiple instances of a metadata repository.
The server implements the Digital Object Interface Protocol specifier by DONA (DOIP).

# Documentation
The specification of Turntable API for Metadata Repcositieries can be found 
[here](https://nfdi4ing.pages.rwth-aachen.de/s-3/s-3-3/turntable-interface/).

# Acknowledgements
The authors would like to thank the Federal Government and the Heads of Government 
of the Länder, as well as the Joint Science Conference (GWK), for their funding and 
support within the framework of the NFDI4Ing consortium. 
Funded by the German Research Foundation (DFG) - project number 442146713. 

# Links
- [DOIP Specification] (https://www.dona.net/sites/default/files/2018-11/DOIPv2Spec_1.pdf)
- [DOIP SDK] (https://www.dona.net/sites/default/files/2020-09/DOIPv2SDKOverview.pdf)
