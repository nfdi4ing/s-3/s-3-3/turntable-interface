## Create a Schema/Application Profile
While creating a schema/AP the DO has to contain
two elements:
1. Serialization of metadata ([DataCite (JSON)](https://github.com/datacite/schema/tree/master/source/json/kernel-4.3))
   - id = metadata
   - type = application/json   
2. Serialization of schema/AP
   - id = application_profile | schema
   - type = application/ld+json | application/json | application/xml
   
### Metadata
| Entry             | Provided | Mandatory | Description |
| :---------------- | :------: | :-------: | :---------- |
| ***identifiers*** | no       | no        | Will be intantiated by the server |
| ***creators***    | yes      | no        | If no creator is provided it will be filled with the entry found in authentication |
| ***titles***      | yes      | yes       | Title of the schema/application profile |
| ***publisher***   | yes      | yes       | Publisher of the schema/application profile  |
| ***publicationYear*** | yes  | no        | If not provided actual year will be set |
| ***formats***     | yes      | yes       | Mimetype of the content. |
|                   |          |           | Possible values:         |
|                   |          |           | - application/ld+json    |
|                   |          |           | - application/json       |
|                   |          |           | - application/xml        |
| ***schemaVersion*** | yes    | no        | If not provided actual kernel version will be set |


### formats
Defines the format of the schema
  - application/ld+json -> Application Profile
  - application/json    -> JSON Schema
  - application/xml     -> XSD


### Minimal DataCite Example (Ingesting an XSD document) 
The DataCite metadata must contain the following fields:
```
{
  "identifiers": [
    {
      "identifierType": "URL",
      "identifier": "https://example.org/metastore/metadata/identifier"
    }
  ],
  "creators": [
    {
      "name": "Smith, Jane"
    }
  ],
  "titles": [
    {
      "title": "Minimal example for a(n) (XSD) schema document"
    }
  ],
  "publisher": "NFDI4Ing",
  "publicationYear": "2021",
  "formats": [
    "application/xml"
  ],
  "types": {
    "resourceTypeGeneral": "Other",
    "resourceType": "XML"
  },
  "schemaVersion": "http://datacite.org/schema/kernel-4"
}

```
  
