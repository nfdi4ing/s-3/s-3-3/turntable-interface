# DataCite Schema

## Forword 
The DataCite Metadata Schema defines a list of core metadata properties to
enable an accurate and consistent identification of a resource for citation 
and retrieval purposes.(schema.datacite.org)

## Specification
For describing the metadata documents and the metadata schema documents/application profiles the 
[DataCite JSON schema](https://github.com/datacite/schema/tree/master/source/json/kernel-4.3) is used.
See examples on next pages for minimal DataCite JSON documents.

## Metadata of a Metadata Document
The metadata or a metadata document may contain the following fields:

| Entry                    | Description                                           |
| :----------------------- | :---------------------------------------------------- |
| ***identifiers***        | Identifier of the DO                                  |
| ***creators***           | Creator of the DO                                     |
| ***titles***             | Title of the DO                                       |
| ***publisher***          | Publisher of the DO                                   |
| ***publicationYear***    | Publication year of the DO                            |
| ***formats***            | Mimetype of the content.                              |
| ***relatedIdentifiers*** | Related DOs.                                          |
|                          | Mandatory relation types:                             |
|                          | - IsMetadataFor: Identifier of the data               |
|                          | - IsDescribedBy: Identifier of the schema/AP          |
| ***schemaVersion***      | expected value: "http://datacite.org/schema/kernel-4" |

## Metadata of a Schema Document / Application Profile
The metadata or a schema document/application profile may contain the following fields:

| Entry                 | Description                                           |
| :-------------------- | :---------------------------------------------------- |
| ***identifiers***     | Identifier of the DO                                  |
| ***creators***        | Creator of the DO                                     |
| ***titles***          | Title of the DO                                       |
| ***publisher***       | Publisher of the DO                                   |
| ***publicationYear*** | Publication year of the DO                            |
| ***formats***         | Mimetype of the content.                              |
| ***schemaVersion***   | expected value: "http://datacite.org/schema/kernel-4" |




