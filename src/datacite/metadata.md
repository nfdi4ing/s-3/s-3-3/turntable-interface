## Create a Metadata Document

While creating a metadata document the DO has to contain
two elements:
1. Serialization of metadata ([DataCite (JSON)](https://github.com/datacite/schema/tree/master/source/json/kernel-4.3))
   - id = metadata
   - type = application/json   
2. Serialization of metadata (document)
   - id = document
   - type = application/ld+json | application/json | application/xml

### Metadata
| Entry             | Provided | Mandatory | Description |
| :---------------- | :------: | :-------: | :---------- |
| ***identifiers*** | no       | no        | Will be intantiated by the server |
| ***creators***    | yes      | no        | If no creator is provided it will be filled with the entry found in authentication |
| ***titles***      | yes      | yes       | Title of the metadata document |
| ***publisher***   | yes      | yes       | Publisher of the metadata document  |
| ***publicationYear*** | yes  | no        | If not provided actual year will be set |
| ***formats***     | yes      | yes       | Mimetype of the content. |
|                   |          |           | Possible values:         |
|                   |          |           | - application/ld+json    |
|                   |          |           | - application/json       |
|                   |          |           | - application/xml        |
| ***relatedIdentifiers*** | yes | yes (both) | Related DOs.                              |
|                   |          |           | Supported relation types:                    |
|                   |          |           | - IsMetadataFor: Identifier of the data      |
|                   |          |           | - IsDescribedBy: Identifier of the schema/AP |
| ***schemaVersion*** | yes    | no        | If not provided actual kernel version will be set |
### formats
Defines the format of the schema
  - application/ld+json -> JSON LD
  - application/json    -> JSON
  - application/xml     -> XSD
  
### Minimal DataCite Examples (Ingesting an XML document) 
The DataCite metadata must contain the following fields:
```
{
  "identifiers": [
    {
      "identifierType": "URL",
      "identifier": "https://example.org/metastore/metadata/identifier"
    }
  ],
  "creators": [
    {
      "name": "Smith, Jane"
    }
  ],
  "titles": [
    {
      "title": "Minimal example for a(n) (XML) metadata document"
    }
  ],
  "publisher": "NFDI4Ing",
  "publicationYear": "2021",
  "formats": [
    "application/xml"
  ],
  "types": {
    "resourceTypeGeneral": "Other",
    "resourceType": "XML"
  },
  "relatedIdentifiers": [
    {
      "relatedIdentifier": "https://example.org/datarepository/identifier",
      "relatedIdentifierType": "URL",
      "relationType": "IsMetadataFor",
    }, 
    {
      "relatedIdentifier": "https://example.org/metastore/schema/identifier",
      "relatedIdentifierType": "URL",
      "relationType": "IsDescribedBy",
    }
  ],
  "schemaVersion": "http://datacite.org/schema/kernel-4"
}

```

