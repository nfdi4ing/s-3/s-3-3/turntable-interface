# Summary

[About](./about.md)
[MIT License](license.md)
# Specification of the Interface
- [DOIP](doip/doip.md)
  - [Operations](doip/operations.md)
    - [Basic Operations](doip/basicOperations.md)
    - [Extended Operations](doip/extendedOperations.md)

---
# Appendix
- [DataCite](datacite/introduction.md)
- [Examples](datacite/examples.md)
  - [Creating a Schema/AP](datacite/schema.md)
  - [Creating a Metadata Document](datacite/metadata.md)
- [Links](links.md)
