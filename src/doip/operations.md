# Operations

This chapter describes the possible operations.
It is splitted in two parts:
- Basic Operations
  Operations which has to be implemented by all metadata repositories.
- Extended Operations
  Operations which may be implemented by a metadata repository.

