# Specification of the Interface

## Forword 
The interface implements the Digital Object Identifier Protocol ([DOIP](https://www.dona.net/sites/default/files/2018-11/DOIPv2Spec_1.pdf)).
It specifies one additional operation due to validate metadata documents.
Implementation is based on [SDK](https://www.dona.net/sites/default/files/2020-09/DOIPv2SDKOverview.pdf) provided by DONA foundation.

## Specification
The DataCite Schema ([JSON Schema](https://github.com/datacite/schema/tree/master/source/json/kernel-4.3)) is used for the metadata of the Digital Objects(DOs). 
