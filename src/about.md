<div class="centerbox">
© RWTH Aachen University
</div>
<div class="centerbox">
© Karlsruhe Institute of Technology
</div>

---
<div class="centerbox">
Version: 1.0
</div>
<div class="centerbox">
Last Upate: 20.12.2023
</div>

---

# About


This document specifies the Turntable API for Metadata Repositories.
The Turntable API acts as a facade for (multiple) (metadata) repositories.

## Target audience

This specification is targeted at people who:

- build clients that should work with multiple (metadata) repositories.

# About us
[Research Data Management](https://www.rwth-aachen.de/cms/root/Forschung/~lnaw/Forschungsdatenmanagement/lidx/1/) is part of the RWTH Aachen University.

[Data Expoitation Methods (DEM)](https://www.scc.kit.edu/ueberuns/dem.php), is part of the Scientific Computing Center (SCC), located at Karlsruhe Institute of Technology (KIT).

<div class="centerbox">
    <div class="logo-spacing-general">
        <a class="logo-align-middle logo-card"
        href="https://www.rwth-aachen.de/"
        title="RWTH Aachen University
        aria-label="Visit the RWTH Aachen University Website">
            <img class="lightonly" src="RWTH_Logo_3.svg" alt="KIT logo">
            <img class="darkonly" src="RWTH_Logo_3.svg" alt="KIT logo">
            <img class="whiteonly" src="RWTH_Logo_3.svg" alt="KIT logo">
        </a>
    </div>
    <div class="logo-spacing-general">
        <a class="logo-align-middle logo-card"
        href="https://kit.edu/"
        title="Karlsruhe Institute of Technology"
        aria-label="Visit the KIT Website">
            <img class="lightonly" src="kit_logo_en_black.svg" alt="KIT logo">
            <img class="darkonly" src="kit_logo_en_white.svg" alt="KIT logo">
            <img class="whiteonly" src="kit_logo_en.svg" alt="KIT logo">
        </a>
    </div>
</div>
